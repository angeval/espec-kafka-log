package com.itau.kafka.consumerlog;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class LogConsumer {

    @KafkaListener(topics = "spec2-angela-valentim-1", groupId = "angela-kafka")
    public void receber(@Payload String log) throws IOException {
        FileWriter fileWriter = new FileWriter("/home/a2/kafka/log_kafka.csv", true);
        try (PrintWriter writer = new PrintWriter(fileWriter)) {
            writer.append(log + "\n");
        }
    }
}