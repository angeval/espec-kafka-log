package com.itau.kafka.consumerlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerlogApplication.class, args);
	}

}
